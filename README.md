INTRODUCTION
------------

This module provides a new media source type that allows to embed a video inside an iframe, and reuse it usign the media
library. It depends on contributed module iframe.

REQUIREMENTS
------------

* This module requires three other modules: media, media_library and iframe.

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
  for further information.

CONFIGURATION
-------------

Read the documentation to learn how to use the module. https://www.drupal.org/docs/contributed-modules/iframe-media-embed-video

MAINTAINERS
-----------

* Cristian García (clgarciab)
* Juan Natera (jncruces)
* James Wilson (jwilson3)
