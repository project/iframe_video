<?php

namespace Drupal\iframe_video\Plugin\media\Source;

use Drupal\Core\Form\FormStateInterface;
use Drupal\media\Annotation\MediaSource;
use Drupal\media\MediaSourceBase;

/**
 * Provides a media source plugin for iframe video resources.
 *
 * @MediaSource(
 *   id = "media_iframe_video",
 *   label = @Translation("Iframe Video"),
 *   description = @Translation("Configure a video incrusted in an iframe."),
 *   allowed_field_types = {"iframe"}
 * )
 *
 *
 */
class IframeVideo extends MediaSourceBase {


  public function getMetadataAttributes() {
    return [];
  }

}
